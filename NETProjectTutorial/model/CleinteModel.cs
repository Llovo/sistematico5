﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{

   
    class CleinteModel
    {
        private static List<Cliente> clientes = new List<Cliente>();

        public static List<Cliente> GetAllClientes()
        {
            return clientes;
        }

        public static void Populate()
        {
            clientes = JsonConvert.DeserializeObject<List<Cliente>>
                (System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.clientes_data_json));
        }
    }
}
