﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dscliente;
        private BindingSource bsCliente;

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();

        }

        public DataSet Dscliente { get => dscliente; set => dscliente = value; }

        private void TxtFindCliente_TextChanged(object sender, EventArgs e)
        {
            bsCliente.Filter = string.Format("Cedula like '*{0}*' or Nombre like '*{0}*' or Apellido like '*{0}*' or Correo like '*{0}*' ", txtFindCliente.Text);

        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = Dscliente;
            bsCliente.DataMember = Dscliente.Tables["Cliente"].TableName;
            dgvClientes.DataSource = bsCliente;
            dgvClientes.AutoGenerateColumns = true;



        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fp = new FrmCliente();
            fp.DtCliente = Dscliente.Tables["Cliente"];
            fp.DsCliente = Dscliente;
            fp.ShowDialog();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fp = new FrmCliente();
            fp.DtCliente = Dscliente.Tables["Cliente"];
            fp.DsCliente = Dscliente;
            fp.DrCliente = drow;
            fp.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Dscliente.Tables["Cliente"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}
