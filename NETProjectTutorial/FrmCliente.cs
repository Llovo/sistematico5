﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {


        private DataTable dtCliente;
        private DataSet dsCliente;
        private BindingSource bsCliente;
        private DataRow drCliente;

        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();

        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
               // txtINSS.Text = drEmpleados["INSS"].ToString();
                txtCed.Text = drCliente["Cedula"].ToString();
                txtNombres.Text = drCliente["Nombre"].ToString();
                txtApellidos.Text = drCliente["Apellido"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
                txtConvencional.Text = drCliente["Telefono"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                //txtDireccion.Text = drCliente["Dirección"].ToString();
              //  cmbSexo.SelectedItem = drCliente["Sexo"].ToString();
            }
        }

        public DataTable DtCliente { get => dtCliente; set => dtCliente = value; }
        public DataSet DsCliente { get => dsCliente; set => dsCliente = value; }



        private void BtnSave_Click(object sender, EventArgs e)
        {
            string cedula, nombres, apellidos, tConvencional, direccion, tCorreo;
             

         //   inss = txtINSS.Text;
            cedula = txtCed.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            direccion = txtDireccion.Text;
            tConvencional = txtConvencional.Text;
            tCorreo = txtCorreo.Text;
          //  salario = double.Parse(txtSalario.Text);
        //    sexo = cmbSexo.SelectedItem.ToString();


            if (drCliente != null)
            {
                DataRow drNew = DtCliente.NewRow();

                int index = DtCliente.Rows.IndexOf(drCliente);
                drNew["Id"] = drCliente["Id"];
               // drNew["INSS"] = inss;
                drNew["Cedula"] = cedula;
                drNew["Nombre"] = nombres;
                drNew["Apellido"] = apellidos;
                drNew["Telefono"] = tConvencional;
                drNew["Correo"] = tCorreo;
                drNew["Direccion"] = direccion;
                //  drNew["Salario"] = salario;
                //    drNew["Sexo"] = sexo;


                DtCliente.Rows.RemoveAt(index);
                DtCliente.Rows.InsertAt(drNew, index);

            }
            else
            {
                DtCliente.Rows.Add(DtCliente.Rows.Count + 1, cedula, nombres, apellidos, tConvencional, tCorreo, direccion);
            }

            Dispose();
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Cliente"].TableName;
        }

    }
}
